package kdg.pos;

/**
 * Created by overvelj on 14/11/2016.
 */
public class Payment {
    private double amount;

    public double getAmount() {
        return amount;
    }

    public Payment(double amount) {
        this.amount = amount;
    }

}
