package kdg.pos;

/**
 * Created by overvelj on 14/11/2016.
 */
public class SalesLineItem {
    private ProductDescription productDescription;
    private int qty;

    public ProductDescription getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(ProductDescription pd) {
        this.productDescription = pd;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public SalesLineItem(ProductDescription pd, int qty) {

        this.productDescription = pd;
        this.qty = qty;
    }

    public double getSubTotal() {
        return productDescription.getPrice()*qty*1;
    }
}
