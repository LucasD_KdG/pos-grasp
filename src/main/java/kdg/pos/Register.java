package kdg.pos;

/**
 * Created by overvelj on 14/11/2016.
 */
public class Register {
    private Sale s;
    private ProductCatalog catalog;
    private Store store;

    public ProductCatalog getCatalog() {
        return catalog;
    }

    public void setCatalog(ProductCatalog catalog) {
        this.catalog = catalog;
    }


    public Sale getSale() {
        return s;
    }

    public void enterItem(int id,int qty){
        ProductDescription itemDesc = catalog.getProductDesc(id);
        s.makeSalesLineItem(itemDesc, qty);
    }

    public Store getStore() {
        return store;
    }

    public Register(ProductCatalog catalog, Store store) {
        this.catalog = catalog;
        this.store = store;

    }

    public void endSale() {
        s.setComplete();
    }

    public double getTotal() {
        return s.getTotal();
    }

    public void makePayment(double i) {
        s.makePayment(i);
        store.addSale(s);
    }

    public void makeNewSale() {
        s = new Sale();
    }
}
