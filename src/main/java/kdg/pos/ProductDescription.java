package kdg.pos;

/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductDescription {
    private int productId;
    private String description;
    private double price;

    public ProductDescription(int productId, String desc, double price) {
        this.productId = productId;
        this.description = desc;
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
