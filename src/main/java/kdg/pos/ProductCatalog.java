package kdg.pos;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductCatalog {
    private Map<Integer, ProductDescription> products;

    public ProductCatalog() {
        this.products = new HashMap<Integer, ProductDescription>();
        this.loadProdSpecs();
    }

    public void addProduct(ProductDescription pd) {
        products.put(pd.getProductId(), pd);
    }

    public ProductDescription getProductDesc(int id) {
        return products.get(id);
    }

    private void loadProdSpecs() {
        // In this class the collection will be loaded from a datasource. For test purposes we just add the needed productSpecifications to the catalog with the addProduct() method.    }

    }
}
