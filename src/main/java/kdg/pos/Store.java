package kdg.pos;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by overvelj on 14/11/2016.
 */
public class Store {
	private List<Sale> sales;
	private  Register register;
	private ProductCatalog catalog;
    public Store() {
        sales = new ArrayList<>();
        catalog = new ProductCatalog();
        register = new Register(catalog,this);
    }

    public void addSale(Sale sale){
        sales.add(sale);
    }

    public int countSales(){
        return sales.size();
    }

    public Register getRegister(){
        return register;
    }
}
